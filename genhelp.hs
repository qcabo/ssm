module Main where

import System.Environment
import System.Exit
import Data.List

mkGeneral :: [Char] -> [Char]
mkGeneral file = concat toc ++ concat (map content items)
    where
        toc = ("### Keywords " ++ top ++ "\n\n") : map (mkkw . fst) items
        content (k, v) = "\n### " ++ k ++ " " ++ top ++ "\n\n" ++ tail (repLinks v) ++ "\n"
        items = [span (/='=') l | l <- lines file, not $ isPrefixOf "#" l, not $ null l]

mkInstr :: [Char] -> [Char]
mkInstr file = concat toc ++ concat (map content items)
    where
        toc = ("### Keywords " ++ top ++ "\n\n") : map (mkkw . fst) items
        content (k, [descr, prepost, example])
            =  "\n\n### " ++ k ++ " " ++ top ++ "\n\n"
            ++ "#### Description\n\n" ++ repLinks descr
            ++ if not (null prepost) then "\n\n#### Pre and postconditions\n\n" ++ pre (repLinks prepost) else ""
            ++ if not (null example) then "\n\n#### Examples\n\n" ++ pre (repLinks example) else ""
            ++ "\n\n"

        pre x = "```\n" ++ commaNewLine x ++ "\n```"
        commaNewLine (',':' ':rest) = '\n':commaNewLine rest
        commaNewLine (x:rest) = x:commaNewLine rest
        commaNewLine [] = []

        items = mkItems $ splitTopic [] $ lines file

        mkItems (a@[descr,prepost,example]:rest) = (kw, map body a) : mkItems rest
            where
                kw = takeWhile (/='_') descr
                body = tail . dropWhile (/='=')
        mkItems (_:xs) = mkItems xs
        mkItems [] = []

        splitTopic :: [String] -> [String] -> [[String]]
        splitTopic acc [] = [reverse acc]
        splitTopic acc ([]:rest) = reverse acc : splitTopic [] rest
        splitTopic acc (x:rest) = splitTopic (x:acc) rest

repLinks [] = []
repLinks ('@':rest) = let (l, r) = span (/=' ') rest in "See " ++ mkkw l ++ repLinks (if null r then [] else tail r)
repLinks (x:xs) = x:repLinks xs

mkkw :: [Char] -> [Char]
mkkw v = concat ["[", v, "](#", v, ") "]

top = "[⭱](#ssm-help)"

main = getArgs >>= \c->case c of
    [general,instr]
        -> putStrLn "# SSM Help\n"
        >> putStrLn "## Table of Contents\n"
        >> putStrLn "- [General help](#General)"
        >> putStrLn "- [Instructions help](#Instructions)\n"
        >> putStrLn "## General\n"
        >> readFile general >>= putStrLn . mkGeneral
        >> putStrLn "## Instructions\n"
        >> readFile instr >>= putStrLn . mkInstr
        >> exitWith ExitSuccess
    _
        -> putStrLn ("Usage: genhelp ssmhelp.prop ssminstrhelp.prop")
        >> exitWith (ExitFailure 1)
