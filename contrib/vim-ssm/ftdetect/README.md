# vim-ssm

## Features

- Syntax highlighting for `*.ssm` files

## How to install

- Pathogen
	Copy the contents of the directory to `~/.vim/bundle/vim-spl.git`
- Vim pack (>8.0)
	Copy the contents of the directory to `~/.vim/pack/plugins/start/vim-ssm`
